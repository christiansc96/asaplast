<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $guarded = [];

    public function store()
    {
    	return $this->hasMany('App\Store', 'state_id');
    }

}
