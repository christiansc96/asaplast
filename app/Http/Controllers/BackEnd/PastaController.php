<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pasta;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;

class PastaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pastas.pastas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pasta = new Pasta();

        $pasta->name = trim($request->name);

        $pasta->active = 1;

        $pasta->save();

        return 'Pasta creado!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pasta = Pasta::findOrFail($id);

        return $pasta;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pasta = Pasta::findOrFail($id);

        $pasta->name = trim($request->name);

        $pasta->active = 1;

        $pasta->save();

        return 'Pasta actualizado!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pasta = Pasta::findOrFail($id);

        $message = 'Pasta '. $pasta->name .' removido';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $pasta->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        return $message;
    }

    /**
     * [status description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function status($id)
    {

        $pasta = Pasta::findOrFail($id);

        if($pasta->active == 1):

            $pasta->active = 0;

            $pasta->save();

            $textstatus = 'Deshabilitado';

        else:

            $pasta->active = 1;

            $pasta->save();

            $textstatus = 'Habilitado';

        endif;

        return 'Pasta '.$pasta->name.' '.$textstatus;

    }

    /**
     * [datatable description]
     * @param  Datatables $datatables [description]
     * @return [type]                 [description]
     */
    public function datatable(Datatables $datatables){

        $pastas = Pasta::all();

        return $datatables->of($pastas)
            ->addColumn('estatus', function($pasta){
                if($pasta->active == 1):

                    return '<span class="label label-sm label-success"> Habilitado </span>';

                endif;

                return '<span class="label label-sm label-warning"> Deshabilitado </span>';
            })
            ->addColumn('opciones', function($pasta){
                if($pasta->active == 1):
                    return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Opciones
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$pasta->id.')">
                                                <i class="fa fa-edit"></i> Editar
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Disable" onclick="statusData('.$pasta->id.')">
                                                <i class="fa fa-times"></i> Deshabilitar 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$pasta->id.')">
                                                <i class="fa fa-trash"></i> Remover 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    else:
                        return '<div class="btn-group pull-right">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Opciones
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="javascript:;" title="Edit" onclick="editForm('.$pasta->id.')">
                                                <i class="fa fa-edit"></i> Editar
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Enable" onclick="statusData('.$pasta->id.')">
                                                <i class="fa fa-check"></i> Habilitar 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" title="Remove" onclick="removeData('.$pasta->id.')">
                                                <i class="fa fa-trash"></i> Remover 
                                            </a>
                                        </li>
                                    </ul>
                                </div>';
                    endif;
            })->rawColumns(['estatus', 'opciones' ])->make(true);
    }
}
