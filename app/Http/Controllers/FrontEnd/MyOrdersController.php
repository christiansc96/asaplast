<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class MyOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where([['user_id', '=',auth()->user()->id], ['active', '<>', 3]])->get();

        return view('frontend.my-orders', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order();

        $order->code = $this->rand_code("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", 5);

        $order->active = 2;

        $order->user_id = auth()->user()->id;

        $order->save();

        // if(auth()->user()->roles[0]->id == 2):
            
        //     $order->asociados()->attach($request->asociado);

        // endif;

        foreach(Cart::instance('tuceramica')->content() as $cartItem):
            
            $product = Product::findOrFail($cartItem->options->product_id);

            for($i=0; $i < $cartItem->qty; $i++){
                $order->products()->attach($product->id);
            }

        endforeach;       

        Cart::instance('tuceramica')->destroy();

        return 'Solicitud de pedido enviada exitosamente!';
    }

    /**
     * Random code to order
     *
     * @param $chars
     * @param $long
     * @return $code
     */
    public function rand_code($chars, $long)
    {

        $code = "";

        for ($x = 0; $x <= $long; $x++) {

            $rand = rand(1, strlen($chars));

            $code .= substr($chars, $rand, 1);

        }

        return $code;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $order->active = trim($request->estatus);

        $order->save();

        return 'Pedido anulado!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);

        $message = 'Pedido '. $order->code .' removido';

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        $order->products()->detach();

        $order->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        return $message;
    }
}
