<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowCase extends Model
{
	public function image()
	{
		return $this->belongsTo('App\Image', 'image_id');
	}

    protected $table = 'show_cases';

    protected $guarded = [];
}
