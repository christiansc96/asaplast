<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $guarded = [];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function image()
    {
    	return $this->belongsTo('App\Image', 'image_id');
    }

    public function state()
    {
    	return $this->belongsTo('App\State', 'state_id');
    }
}
