<?php

use Illuminate\Database\Seeder;
use App\Space;

class SpaceSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $space = new Space();
        $space->name = 'sala';
        $space->active = 1;
        $space->save();

        $space = new Space();
        $space->name = 'cocina';
        $space->active = 1;
        $space->save();

        $space = new Space();
        $space->name = 'baños';
        $space->active = 1;
        $space->save();

        $space = new Space();
        $space->name = 'comedor';
        $space->active = 1;
        $space->save();

        $space = new Space();
        $space->name = 'habitación';
        $space->active = 1;
        $space->save();

        $space = new Space();
        $space->name = 'oficinas';
        $space->active = 1;
        $space->save();

        $space = new Space();
        $space->name = 'exteriores';
        $space->active = 1;
        $space->save();

    }
}
