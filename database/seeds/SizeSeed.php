<?php

use Illuminate\Database\Seeder;
use App\Size;

class SizeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $size = new Size();
        $size->name = '30 cm';
        $size->active = 1;
        $size->save();

        $size = new Size();
        $size->name = '45 cm';
        $size->active = 1;
        $size->save();

        $size = new Size();
        $size->name = '60 cm';
        $size->active = 1;
        $size->save();

        $size = new Size();
        $size->name = '75 cm';
        $size->active = 1;
        $size->save();

        $size = new Size();
        $size->name = '90 cm';
        $size->active = 1;
        $size->save();
    }
}
