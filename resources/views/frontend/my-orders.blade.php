@extends('layouts.front')
@section('title', 'Mis pedidos')
@section('styles')
@endsection
@section('content')

<!-- Header End -->

<!-- Page Header Begins -->

<div id="page-header">
    <div class="header-bg-parallax" style="margin-top: -90px;">
        <div class="overlay">
            <div class="container text-center">
                <div class="header-description">
                    <h1>Mis Pedidos</h1>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('fe.home') }}">Home</a>
                            </li>
                            <li><a href="{{ route('mis-pedidos.index') }}">Mis pedidos</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /header-small-nav -->
                </div>
                <!-- /header-description -->
            </div>
            <!-- /container -->
        </div>
        <!-- /overlay -->
    </div>
    <!-- /product-filter -->
</div>

<!-- Page Header End -->

<!-- Cart Table Begin -->

<div class="cart-table">
    <div class="container padding-vertical-100">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table" id="order-table">
                        <thead>
                            <tr>
                                <th class="text-center">Código</th>
                                <th class="text-center">Descripción</th>
                                <th class="text-center">Estatus</th>
                                <th class="text-center">Productos</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <!-- /thead -->

                        <tbody>
                          @foreach($orders as $order)
                            {{-- expr --}}
                            <tr id="{{$order->id}}">
                                <td class="text-center">
                                    {{$order->code}}
                                </td>
                                <td class="product-name text-center">
                                    <p>{{$order->description}}</p>
                                </td>
                                <!-- /product-name -->
                                <td class="product-name text-center">
                                    <p>
                                        @if ($order->active == 1)
                                            Aprobado
                                        @elseif($order->active == 2)
                                            En espera
                                        @else
                                            Rechazado
                                        @endif
                                    </p>
                                </td>
                                <!-- /product-price -->
                                <td class="text-center">
                                    {{$order->products->count()}}
                                </td>
                                <!-- /product-price -->
                                <td class="text-center">
                                    <a class="btn-close" href="#" onclick="deleteData({{$order->id}})"><i class="fa fa-close"></i></a>
                                </td>
                                <!-- /btn-close -->
                            </tr>
                          @endforeach
                        </tbody>
                        <!-- /tbody -->
                    </table>
                    <!-- /table -->
                </div>
                <!-- /table-responsive -->

            </div>
            <!-- /column -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
{!!Form::open(['route'=>[ 'mis-pedidos.update', ':MYORDER_ID' ],'method'=>'PATCH', 'id' => 'form-update-order'])!!}
{!!Form::close()!!}
@endsection
@section('scripts')
<script>

var deleteData = function(id){

  var form = $('#form-update-order');

  var url = form.attr('action').replace(':MYORDER_ID', id);

  var data = form.serialize()+'&estatus=3';

  swal({
    title: 'Estás seguro?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    cancelButtonColor: '#d33',
    confirmButtonColor: '#3085d6',
    confirmButtonText: 'Si',
    cancelButtonText: 'No',
  }).then(function () {
    $.post(url, data, function(response){
      $("#order-table").load(" #order-table");
      swal({
          title: 'Aprobado!',
          text: response,
          type: 'success',
          timer: '1500'
      })
    });
  });

}
</script>
@endsection