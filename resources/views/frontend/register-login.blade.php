@extends('layouts.front')

@section('styles')
<link href="{{asset('assets/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet">
@endsection

@section('content')

<!-- Header End -->

<!-- Page Header Begins -->

<div id="page-header">
    <div class="header-bg-parallax" style="margin-top: -90px;">
        <div class="overlay">
            <div class="container text-center">
                <div class="header-description">
                    <h1>Registarse | Iniciar Sesión</h1>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="{{ route('fe.home') }}">home</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /header-small-nav -->
                </div>
                <!-- /header-description -->
            </div>
            <!-- /container -->
        </div>
        <!-- /overlay -->
    </div>
    <!-- /product-filter -->
</div>

<!-- Page Header End -->

<!-- Cart Table Begin -->

<div class="cart-table" id="clients">
    <div class="container padding-vertical-100">
        <div class="row">
            <div class="col-md-6">
                <h6>Registrate</h6>
                <p class="padding-vertical-25" style="line-height: 150%;">Si aun no tienes cuenta puedes crear una y registrarte para optener todos los beneficios que te ofrece Tuceramica.com.</p>

                <form class="cart-form padding-top-35 padding-bottom-70" action="{{ route('fe.register') }}" data-vv-scope="form-1" id="form-post-register">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group padding-bottom-5" :class="{'has-error': errors.has('form-1.name') }">
                                <label for="name">Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre Apellido" required name="name" v-validate="'required|alpha_spaces|max:255'">
                                <span v-show="errors.has('form-1.name')" style="color: red;" class="help is-danger">
                                @{{ errors.first('form-1.name') }}
                            </span>
                            </div>
                            <!-- /form-group -->
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.email') }">
                                <label for="email">Email</label>
                                <input type="text" name="email" class="form-control" placeholder="Mi_correo@gmail.com" required v-model="email" v-validate="'required|email|max:255'">
                                <span v-show="errors.has('form-1.email')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.email') }}
                                </span>
                            </div>
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.phone') }">
                                <label for="phone">Telefono</label>
                                <input type="text" name="phone" class="form-control" placeholder="+58 123 4567" required v-model="phone" v-validate="{required: true, max: 255, regex: /\(?([0-9\-\+]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/}">
                                <span v-show="errors.has('form-1.phone')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.phone') }}
                                </span>
                            </div>
                            <!-- /form-group -->
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.fiscal_address') }">
                                <label for="fiscal_address">Dirección fiscal:</label>
                                <input type="text" name="fiscal_address" class="form-control" placeholder="Dirección fiscal" required v-model="fiscal_address" v-validate="'required|alpha_spaces|max:255'"> 
                                <span v-show="errors.has('form-1.fiscal_address')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.fiscal_address') }}
                                </span>
                            </div>
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.store_address') }">
                                <label for="store_address">Dirección tienda</label>
                                <input type="text" name="store_address" class="form-control" placeholder="Dirección tienda" required v-model="store_address" v-validate="'alpha_spaces|max:255'">
                                <span v-show="errors.has('form-1.store_address')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.store_address') }}
                                </span>
                            </div>
                            <!-- /form-group -->
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.deposit_address') }">
                                <label for="deposit_address">Dirección deposito:</label>
                                <input type="text" name="deposit_address" class="form-control" placeholder="Dirección deposito" required v-model="deposit_address" v-validate="'alpha_spaces|max:255'">
                                <span v-show="errors.has('form-1.deposit_address')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.deposit_address') }}
                                </span>
                            </div>
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.contact_person') }">
                                <label for="contact_person">Persona de contacto</label>
                                <input type="text" name="contact_person" class="form-control" placeholder="Nombre Apellido" required v-model="contact_person" v-validate="'required|alpha_spaces|max:255'">
                                <span v-show="errors.has('form-1.contact_person')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.contact_person') }}
                                </span>
                            </div>
                            <!-- /form-group -->
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.contact_phone') }">
                                <label for="contact_phone">Telefono persona de contacto:</label>
                                <input type="text" name="contact_phone" class="form-control" placeholder="+58 123 4567" required v-model="contact_phone" v-validate="{required: true, max: 255, regex: /\(?([0-9\-\+]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/}">
                                <span v-show="errors.has('form-1.contact_phone')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.contact_phone') }}
                                </span>
                            </div>
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.password') }">
                                <label for="password">Contraseña:</label>
                                <input type="password" name="password" class="form-control" placeholder="*********" required v-model="password" v-validate="'required|alpha_num|max:18|min:6'">
                                <span v-show="errors.has('form-1.password')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.password') }}
                                </span>
                            </div>
                            <div class="form-group coupon-code margin-top-25" :class="{'has-error': errors.has('form-1.confirm_password') }">
                                <label for="confirm_password">Confirmar contraseña:</label>
                                <input type="password" name="confirm_password" class="form-control" placeholder="*********" required v-model="confirm_password" v-validate="'confirmed:password'">
                                <span v-show="errors.has('form-1.confirm_password')" style="color: red;" class="help is-danger">
                                    @{{ errors.first('form-1.confirm_password') }}
                                </span>
                            </div>
                            <!-- /form-group -->
                            <div class="row padding-top-20">
                                <div class="col-md-6 col-sm-6 pull-right">
                                    <button type="button" @click="register_client()" class="btn-cart pull-right">Registrarme</button>
                                </div>
                                <!-- /column -->
                            </div>
                            <!-- /row -->
                        </div>
                        <!-- /column -->
                    </div>
                    <!-- /row -->
                </form>
                <!-- /cart-form -->
            </div>
            <!-- /column -->

            <div class="col-md-6">
                <h6>Ingresar</h6>
                <p class="padding-vertical-25" style="line-height: 150%;">Si ya tienes cuenta ingresa con tu email y contraseña</p>
                @if (session('status'))
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif
                <form class="cart-form padding-top-25 padding-bottom-70" method="POST" action="{{ route('fe.login') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="padding-bottom-5 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Mi_correo@gmail.com" >
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- /form-group -->
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} coupon-code margin-top-25">
                                <label for="password">Contraseña:</label>
                                <input type="password" class="form-control" name="password" placeholder="*********" >
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- /form-group -->
                            <div class="form-group login-options margin-bottom-20">
                                <label>
                                    <div class="squaredFour">
                                        <input type="checkbox" value="None" id="squaredFour" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="squaredFour"></label>
                                    </div>
                                    <!-- /squaredFour -->
                                    <p>Mantenerme logueado</p>
                                    <a href="#">Olvidé mi contraseña</a>
                                </label>
                                <!-- /label -->
                            </div>
                            <!-- /login-options -->
                            <button type="submit" class="btn-login pull-right">Ingresar a mi cuenta</button>

                        </div>
                        <!-- /column -->
                    </div>
                    <!-- /row -->
                </form>
                <!-- /cart-form -->
            </div>
            <!-- /column -->
        </div>

    </div>
    <!-- /container -->
</div>

<!-- Cart Table End -->
@endsection

@section('scripts')
<script src="https://unpkg.com/vue@latest"></script>
<script src="https://unpkg.com/vee-validate@2.0.0-rc.7/dist/vee-validate.js" type="text/javascript">
</script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
{{-- SweetAlert2 --}}
<script src="{{asset('assets/sweetalert2/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
    Vue.use(VeeValidate);
</script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="{{asset('assets/global/plugins/bootbox/bootbox.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/pages/scripts/ui-bootbox.min.js')}}" type="text/javascript"></script>
<script>
var clients = new Vue({
    el: '#clients',
    data: {
        id: null,
        name: null,
        email: null,
        phone: null,
        fiscal_address: null,
        store_address: null,
        deposit_address: null,
        contact_person: null,
        contact_phone: null,
        password: null,
        confirm_password: null,
        save_method: null,
        method: null,
    },
    methods: {
        register_client: function(){
            this.$validator.validateAll('form-1').then((result) => {
                if(result){

                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    
                    let formData = new FormData();

                    var form = $('#form-post-register');

                    var url = form.attr('action');

                    formData.append('_method', 'POST');
                    formData.append('name', clients.name);
                    formData.append('email', clients.email);
                    formData.append('phone', clients.phone);
                    formData.append('fiscal_address', clients.fiscal_address);
                    formData.append('store_address', clients.store_address);
                    formData.append('deposit_address', clients.deposit_address);
                    formData.append('contact_person', clients.contact_person);
                    formData.append('contact_phone', clients.contact_phone);
                    if(clients.password != null){
                        formData.append('password', clients.password);
                    }
                    
                    axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                    }).then(function (response) {
                        console.log(response);
                        dialog.modal('hide');
                        swal({
                            title: 'Registro exitoso!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        console.log(error);
                        dialog.modal('hide');
                        var refrescar = bootbox.dialog({
                            title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                            message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                            closeButton: false,
                            buttons: {
                                refrescar: {
                                    label: '<i class="fa fa-refresh"></i> Refrescar',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                    return;
                }
                bootbox.alert({
                    message: "<p class='text-center'>Debe corregir los errores</p>",
                    backdrop: true,
                    closeButton: false,
                });
            });
        }
    }
});

</script>
@endsection

