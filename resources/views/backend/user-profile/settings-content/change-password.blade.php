{!!Form::open(['route'=>[ 'profile.user.updatePwd'],'method'=>'POST', 'id' => 'form-update-password', 'data-vv-scope' => 'form-2'])!!}
    <div class="form-group" :class="{'has-error': errors.has('form-2.old_password') }">
        <label class="control-label">
            Current Password
        </label>
        <input class="form-control" type="password" name="old_password" v-model="old_password" v-validate="'required'"/>
        <span v-show="errors.has('form-2.old_password')" style="color: #e73d4a;" class="help">@{{ errors.first('form-2.old_password') }}</span>
    </div>
    <div class="form-group" :class="{'has-error': errors.has('form-2.password') }">
        <label class="control-label">
            New Password
        </label>
        <input class="form-control" name="password" type="password" v-model="password" v-validate="'required|alpha_num|max:18|min:6'"/> 
        <span v-show="errors.has('form-2.password')" style="color: #e73d4a;" class="help">@{{ errors.first('form-2.password') }}</span>
    </div>
    <div class="form-group" :class="{'has-error': errors.has('form-2.confirm_password') }">
        <label class="control-label">
            Re-type New Password
        </label>
        <input class="form-control" name="confirm_password" type="password" v-model="confirm_password" v-validate="'required|confirmed:password'"/> 
        <span v-show="errors.has('form-2.confirm_password')" style="color: #e73d4a;" class="help">@{{ errors.first('form-2.confirm_password') }}</span>
    </div>
    <div class="margin-top-10">
        <a class="btn green" href="javascript:;" @click="updatePassword()">
            Change Password
        </a>
        <a class="btn default" href="javascript:;">
            Cancel
        </a>
    </div>
{!!Form::close()!!}