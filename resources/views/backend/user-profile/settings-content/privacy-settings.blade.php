<form action="#">
    <table class="table table-light table-hover">
        <tr>
            <td>
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus..
            </td>
            <td>
                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input name="optionsRadios1" type="radio" value="option1"/>
                        Yes
                        <span>
                        </span>
                    </label>
                    <label class="mt-radio">
                        <input checked="" name="optionsRadios1" type="radio" value="option2"/>
                        No
                        <span>
                        </span>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                Enim eiusmod high life accusamus terry richardson ad squid wolf moon
            </td>
            <td>
                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input name="optionsRadios11" type="radio" value="option1"/>
                        Yes
                        <span>
                        </span>
                    </label>
                    <label class="mt-radio">
                        <input checked="" name="optionsRadios11" type="radio" value="option2"/>
                        No
                        <span>
                        </span>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                Enim eiusmod high life accusamus terry richardson ad squid wolf moon
            </td>
            <td>
                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input name="optionsRadios21" type="radio" value="option1"/>
                        Yes
                        <span>
                        </span>
                    </label>
                    <label class="mt-radio">
                        <input checked="" name="optionsRadios21" type="radio" value="option2"/>
                        No
                        <span>
                        </span>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                Enim eiusmod high life accusamus terry richardson ad squid wolf moon
            </td>
            <td>
                <div class="mt-radio-inline">
                    <label class="mt-radio">
                        <input name="optionsRadios31" type="radio" value="option1"/>
                        Yes
                        <span>
                        </span>
                    </label>
                    <label class="mt-radio">
                        <input checked="" name="optionsRadios31" type="radio" value="option2"/>
                        No
                        <span>
                        </span>
                    </label>
                </div>
            </td>
        </tr>
    </table>
    <!--end profile-settings-->
    <div class="margin-top-10">
        <a class="btn red" href="javascript:;">
            Save Changes
        </a>
        <a class="btn default" href="javascript:;">
            Cancel
        </a>
    </div>
</form>