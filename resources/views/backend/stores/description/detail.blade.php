<div class="row">

	<div class="col-md-6">
		<p>Nombre: <strong>{{ $store->name }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Rif: <strong>{{ $store->rif }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Dirección: <strong>{{ $store->address }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Teléfono: <strong>{{ $store->phone }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Email: <strong>{{ $store->email }}</strong></p>
	</div>

	<div class="col-md-6">
		<p>Pertenece a: <a href="{{ route('profile.user', $store->user->username)}}"><strong>{{ $store->user->username }}</strong></a></p>
	</div>

	<div class="col-md-6">
		<p>Fecha de creación: {{ $store->created_at }}</p>
	</div>
	
</div>