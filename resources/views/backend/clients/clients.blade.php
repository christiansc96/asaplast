@extends('layouts.back')

@section('title', 'admin clientes')

@section('styles')
{{-- dataTables --}}
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        Clientes
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('be.dashboard') }}">
                            Dashboard
                        </a>
                        <i class="fa fa-circle">
                        </i>
                    </li>
                    <li>
                        <span>
                            Clientes
                        </span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner" id="clients">
                    <div class="portlet box dark">
                        <div class="portlet-title">
                            <div class="caption">
                                Ultimos clientes agregados
                            </div>
                            <a class="btn green-haze btn-outline btn-circle btn-md pull-right" @click="addForm()" style="margin:3px -7px 0 0;">
                                Agregar cliente
                            </a>
                        </div>
                        <div class="portlet-body flip-scroll">
                            <table class="table table-striped table-bordered table-hover table-condensed flip-content" id="client-table">
                                <thead class="flip-content" style="background:#2D3742; color:#FFFFFF;">
                                    <tr>
                                        <th>id</th>
                                        <th>nombre</th>
                                        <th>email</th>
                                        <th>teléfono</th>
                                        <th>dirección fiscal</th>
                                        <th>dirección tienda</th>
                                        <th>dirección deposito</th>
                                        <th>estatus</th>
                                        <th>opciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    @include('backend.clients.form-client')
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
{!!Form::open(['route'=>[ 'superadmin.datatable.status.client', ':CLIENT_ID'],'method'=>'GET', 'id' => 'form-estatus'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-clientes.destroy', ':CLIENT_ID' ],'method'=>'DELETE', 'id' => 'form-delete'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-clientes.edit', ':CLIENT_ID' ],'method'=>'GET', 'id' => 'form-edit'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>[ 'admin-clientes.update', ':CLIENT_ID' ],'method'=>'PATCH', 'id' => 'form-update'])!!}
{!!Form::close()!!}
@endsection

@section('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script>
var clients = new Vue({
    el: '#clients',
    data: {
        id: null,
        name: null,
        email: null,
        phone: null,
        fiscal_address: null,
        store_address: null,
        deposit_address: null,
        contact_person: null,
        contact_phone: null,
        password: null,
        confirm_password: null,
        save_method: null,
        method: null,
    },
    methods: {
        addForm: function(){
            clients.save_method = 'add';
            clients.method = 'POST';
            // $('#role').show();
            // $('#fullname').show();
            // $('#phone').show();
            // $('#email').show();
            // $('#username').show();
            // $('#password').show();
            // $('#confirm_password').show();
            // $('#divider').show();
            $('#form-post').show();
            $('#modal-form').modal('show');
            clients.id = null;
            clients.name = null;
            clients.email = null;
            clients.phone = null;
            clients.fiscal_address = null;
            clients.store_address = null;
            clients.deposit_address = null;
            clients.contact_person = null;
            clients.contact_phone = null;
            clients.password = null;
            clients.confirm_password = null;
            $('.modal-title').text('Agregar cliente');
        },
        modal_form: function(){
            this.$validator.validateAll('form-1').then((result) => {
                if(result){

                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    
                    let formData = new FormData();

                    if (clients.save_method == 'add'){

                        var form = $('#form-post');

                        var url = form.attr('action');

                    }else{

                        var form = $('#form-update');

                        var url = form.attr('action').replace(':CLIENT_ID', clients.id);

                        formData.append('id', clients.id);

                    } 

                    formData.append('_method', clients.method);
                    formData.append('name', clients.name);
                    formData.append('email', clients.email);
                    formData.append('phone', clients.phone);
                    formData.append('fiscal_address', clients.fiscal_address);
                    formData.append('store_address', clients.store_address);
                    formData.append('deposit_address', clients.deposit_address);
                    formData.append('contact_person', clients.contact_person);
                    formData.append('contact_phone', clients.contact_phone);
                    if(clients.password != null){
                        formData.append('password', clients.password);
                    }
                    
                    axios.post(url, formData,{
                        headers: {
                            'Content-Type': 'multipart/form-data, application/json;charset=UTF-8'
                        }
                    }).then(function (response) {
                        console.log(response);
                        dialog.modal('hide');
                        table.ajax.reload();
                        swal({
                            title: 'Aprobado!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                        $('#modal-form').modal('hide');
                    }).catch(function (error) {
                        console.log(error);
                        dialog.modal('hide');
                        var refrescar = bootbox.dialog({
                            title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                            message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                            closeButton: false,
                            buttons: {
                                refrescar: {
                                    label: '<i class="fa fa-refresh"></i> Refrescar',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                    return;
                }
                bootbox.alert({
                    message: "<p class='text-center'>Debe corregir los errores</p>",
                    backdrop: true,
                    closeButton: false,
                });
            });
        }
    }
});

var table = $('#client-table').DataTable({
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "_MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando de _START_ a _END_ de _TOTAL_ entradas",
                    "sInfoEmpty":      "Mostrando de 0 a 0 de 0 entradas",
                    "sInfoFiltered":   "(filtrado de _MAX_ entradas totales)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
              processing: true,
              serverSide: true,
              ajax: "{{ route('superadmin.datatable.clients') }}",
              columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},                
                {data: 'fiscal_address', name: 'fiscal_address'},
                {data: 'store_address', name: 'store_address'},
                {data: 'deposit_address', name: 'deposit_address'},
                // {data: 'contact_person', name: 'contact_person'},
                // {data: 'contact_phone', name: 'contact_phone'},
                {data: 'estatus', name: 'estatus'},
                {data: 'opciones', name: 'opciones', orderable: false, searchable: false}
              ]
            });

function editForm(id)
{
    var form = $('#form-edit');
    var url = form.attr('action').replace(':CLIENT_ID', id);
    var data = form.serialize();
    clients.save_method = 'edit';
    clients.method = 'PATCH';
    axios.get(url, data)
    .then(function (response) {
        console.log(response.data);
        $('#modal-form').modal('show');
        $('.modal-title').text('Editar cliente');
        $('#form-post').show();
        clients.id = response.data.user.id;
        clients.name = response.data.name;
        clients.email = response.data.user.email;
        clients.phone = response.data.user.phone;
        clients.fiscal_address = response.data.user.fiscal_address;
        clients.store_address = response.data.user.store_address;
        clients.deposit_address = response.data.user.deposit_address;
        clients.contact_person = response.data.user.contact_person;
        clients.password = null;
        clients.confirm_password = null;
    }).catch(function (error) {
        console.log(error);
        var refrescar = bootbox.dialog({
            title: "<p class='text-center'>Un error ha ocurrido :(</p>",
            message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
            closeButton: false,
            buttons: {
                refrescar: {
                    label: '<i class="fa fa-refresh"></i> Refrescar',
                    callback: function (result) {
                        location.reload(true);
                    }
                }
            }
        });
    });
}

function statusData(id)
{
    var reactive = bootbox.dialog({
        message: "<p class='text-center'>Está seguro?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    var form = $('#form-estatus');
                    var url = form.attr('action').replace(':CLIENT_ID', id);
                    var data = form.serialize();
                    axios.get(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Estatus actualizado!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refrescar = bootbox.dialog({
                            title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                            message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                            closeButton: false,
                            buttons: {
                                refrescar: {
                                    label: '<i class="fa fa-refresh"></i> Refrescar',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
function removeData(id)
{
    var form = $('#form-delete');
    var url = form.attr('action').replace(':CLIENT_ID', id);
    var data = form.serialize();

    var eliminar = bootbox.dialog({
        message: "<p class='text-center'>Quiere remover este <strong>usuario</strong>?</p>",
        closeButton: false,
        buttons: {
            confirm: {
                label: 'SI',
                className: 'btn-success',
                callback: function () {
                    var dialog = bootbox.dialog({
                        message: '<p class="text-center"><i class="fa fa-spin fa-spinner"></i></p>',
                        closeButton: false
                    });
                    axios.delete(url, data)
                    .then(function (response) {
                        dialog.modal('hide');
                        console.log(response.data);
                        table.ajax.reload();
                        swal({
                            title: 'Removido!',
                            text: response.data,
                            type: 'success',
                            timer: '1500'
                        });
                    }).catch(function (error) {
                        dialog.modal('hide');
                        console.log(error);
                        var refrescar = bootbox.dialog({
                            title: "<p class='text-center'>Un error ha ocurrido :(</p>",
                            message: "<p class='text-center'>Hubo un inconveniente al enviar los datos, intente volver a cargar la página</p>",
                            closeButton: false,
                            buttons: {
                                refrescar: {
                                    label: '<i class="fa fa-refresh"></i> Refrescar',
                                    callback: function (result) {
                                        location.reload(true);
                                    }
                                }
                            }
                        });
                    });
                }
            },
            cancel: {
                label: 'No',
                className: 'btn-danger',
            }
        },
    });
}
</script>
@endsection
